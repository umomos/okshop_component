import 'package:flutter/material.dart';
import 'package:okshop_model/okshop_model.dart';

//上方'內用' block 的形式
enum StickerStyle {
  Filled,
  Outline,
  Max,
}

//標籤貼紙預覽 Widget
class StickerWidget extends StatelessWidget {
  static const DEFAULT_WIDTH = 332.0;
  static const _FONT_SIZE_1 = 32.0;
  static const _FONT_SIZE_2 = 24.0;
  static const _FONT_SIZE_3 = 28.0;
  static const _FONT_SIZE_4 = 30.0;
  final Sticker data;
  final double width;

  StickerWidget({
    @required this.data,
    this.width = DEFAULT_WIDTH,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? DEFAULT_WIDTH,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      padding: EdgeInsets.only(
        left: 16,
        right: 16,
        top: 16,
      ),
      child: SingleChildScrollView(
        physics: const NeverScrollableScrollPhysics(),
        child: MediaQuery(
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: 1.0,
            boldText: false,
          ),
          child: _main(),
        ),
      ),
    );
  }

  Widget _main() {
    final children = <Widget>[];
    children.add(_title());
    children.add(_subtitle());
    children.add(
      Divider(
        height: 12.0,
        thickness: 2.0,
        color: Colors.black,
      ),
    );
    children.addAll(_body());
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: children,
    );
  }

  Iterable<Widget> _body() {
    final children = <Widget>[];
    children.add(
      Text(
        // '厚切腰內豬排厚切腰內豬排厚切腰內豬排厚切腰內豬排',
        data?.bodyTitle ?? '',
        style: TextStyle(
          fontSize: _FONT_SIZE_4,
          color: Colors.black,
          fontWeight: FontWeight.bold,
        ),
        textHeightBehavior: TextHeightBehavior(applyHeightToFirstAscent: false),
        // maxLines: 2,
        overflow: TextOverflow.clip,
        softWrap: true,
      ),
    );
    children.add(
      Text(
        // '/半碗飯/不要太多肉/半碗飯/不要太多肉/半碗飯/不要太多肉/半碗飯/不要太多肉',
        data?.bodySubTitle ?? '',
        style: TextStyle(
          fontSize: _FONT_SIZE_3,
          color: Colors.black,
          // fontWeight: FontWeight.w300,
        ),
        textHeightBehavior: TextHeightBehavior(applyHeightToFirstAscent: false),
        // maxLines: 2,
        overflow: TextOverflow.clip,
        softWrap: true,
      ),
    );
    return children;
  }

  // 時間: 12:33  $8,888
  Widget _subtitle() {
    final children = <Widget>[];
    children.add(
      Expanded(
        child: Text(
          // '時間 12:34'
          data?.headerSubTitle ?? '',
          // style: Theme.of(context).textTheme.headline6,
          style: TextStyle(
            fontSize: _FONT_SIZE_3,
            color: Colors.black,
          ),
          overflow: TextOverflow.clip,
          maxLines: 1,
        ),
      ),
    );
    children.add(SizedBox(width: 8));
    children.add(
      Text(
        // '$8,888',
        data?.headerSubTitleNote ?? '',
        // style: Theme.of(context).textTheme.headline6,
        style: TextStyle(
          fontSize: _FONT_SIZE_3,
          color: Colors.black,
        ),
        textAlign: TextAlign.end,
        maxLines: 1,
        overflow: TextOverflow.clip,
        softWrap: false,
      ),
    );
    return Row(children: children);
  }

  Widget _title() {
    final children = <Widget>[];
    children.add(
      Text(
        // '168',
        data?.headerTitleNum ?? '',
        style: TextStyle(
          fontSize: _FONT_SIZE_1,
          color: Colors.black,
        ),
        overflow: TextOverflow.clip,
        softWrap: false,
        maxLines: 1,
      ),
    );
    children.add(SizedBox(width: 4));
    children.add(
      blockTagByStyle(
        // '內用',
        data?.headerTitleBlockText,
        tagStyle: data?.tagStyle?.tagStyle ?? StickerStyle.Filled,
      ),
    );
    children.add(SizedBox(width: 4));
    children.add(
      Expanded(
        child: Text(
          // '2F01',
          data?.headerTitleNote ?? '',
          style: TextStyle(
            fontSize: _FONT_SIZE_4,
            color: Colors.black,
          ),
          overflow: TextOverflow.clip,
          softWrap: false,
          maxLines: 1,
          textAlign: TextAlign.right,
        ),
      ),
    );
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: children,
    );
  }

  static Widget blockTagByStyle(
    String text, {
    StickerStyle tagStyle,
  }) {
    switch (tagStyle ?? StickerStyle.Filled) {
      case StickerStyle.Filled:
        return filledBlockTag(text);
      case StickerStyle.Outline:
      default:
        return filledBlockTag(text);
    }
  }

  //Filled
  static Widget filledBlockTag(String text) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
      color: Colors.black,
      child: Text(
        text ?? '',
        style: TextStyle(
          fontSize: _FONT_SIZE_2,
          color: Colors.white,
        ),
        overflow: TextOverflow.clip,
        softWrap: false,
        maxLines: 1,
      ),
    );
  }

  // Outline
  static Widget outlineBlockTag(String text) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 6, vertical: 2),
      decoration: BoxDecoration(
        color: Colors.transparent,
        border: Border.all(width: 1),
      ),
      child: Text(
        // '內用',
        text ?? '',
        style: TextStyle(
          fontSize: _FONT_SIZE_2,
          color: Colors.black,
          // fontWeight: FontWeight.bold,
        ),
        overflow: TextOverflow.clip,
        softWrap: false,
        maxLines: 1,
      ),
    );
  }
}

extension _ExtensionNum on num {
  StickerStyle get tagStyle {
    if (this != null && this >= 0 && this < StickerStyle.values.length) {
      return StickerStyle.values.elementAt(this);
    }
    return StickerStyle.Max;
  }
}
