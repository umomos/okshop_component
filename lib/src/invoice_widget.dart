import 'package:barcode/barcode.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:okshop_model/okshop_model.dart';

class InvoiceWidget extends StatelessWidget {
  static const _TITLE_FONT_SIZE = 32.0;
  static const _DEF_FONT_SIZE = 26.0;
  final Invoice data;
  final _dm = Barcode.code39();
  final _qr = Barcode.qrCode(typeNumber: 6);

  InvoiceWidget({
    Key key,
    @required this.data,
  }) : super(key: key);

  ///
  /// ref: https://stackoverflow.com/a/52139430
  ///
  @override
  Widget build(BuildContext context) {
    const mm58 = true;
    return Container(
      width: mm58 ? 372 : 558,
      padding: EdgeInsets.zero,
      decoration: BoxDecoration(
        color: Colors.white,
      ),
      child: SingleChildScrollView(
        physics: NeverScrollableScrollPhysics(),
        child: MediaQuery(
          data: MediaQuery.of(context).copyWith(
            textScaleFactor: 1.0,
            boldText: false,
          ),
          child: _main(),
        ),
      ),
    );
  }

  Widget _main() {
    final children = <Widget>[];
    children.add(_storeName());
    children.add(_printMark());
    children.add(_yearMonth());
    children.add(_invoiceNumber());
    children.add(_dateTime());
    children.add(_randomAndTotal());
    children.add(_sellerAndBuyer());
    children.add(_divider());
    children.add(_barCode());
    children.add(_divider());
    children.add(_doubleQrCode());
    return Column(
      mainAxisSize: MainAxisSize.min,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: children,
    );
  }

  Widget _storeName() {
    return Text(
      // '企業人企業識別標章'
      data.storeName ?? '',
      style: const TextStyle(
        fontSize: _TITLE_FONT_SIZE,
        color: Colors.black,
      ),
      maxLines: 2,
      softWrap: true,
      textAlign: TextAlign.center,
    );
  }

  Widget _printMark() {
    return Text(
      data.displayPrintMark ?? '',
      maxLines: 1,
      softWrap: false,
      style: TextStyle(
        fontSize: true == data.isPrinted ? 30.0 : 36.0,
        color: Colors.black,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _yearMonth() {
    return Text(
      data.displayTwDateTime ?? '',
      maxLines: 1,
      softWrap: false,
      style: const TextStyle(
        fontSize: 40.0,
        color: Colors.black,
        fontWeight: FontWeight.w700,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _invoiceNumber() {
    return Text(
      // 'XX-12345678',
      data.displayInvoiceNumber ?? '',
      maxLines: 1,
      softWrap: false,
      style: const TextStyle(
        fontSize: 40.0,
        color: Colors.black,
        fontWeight: FontWeight.w700,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _dateTime() {
    final children = <Widget>[];
    children.add(
      Expanded(
        child: Text(
          data.displayDateTime ?? '',
          maxLines: 1,
          softWrap: false,
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
        ),
      ),
    );
    children.add(
      Text(
        data.displayTaxType ?? '',
        maxLines: 1,
        softWrap: false,
        style: const TextStyle(
          fontSize: _DEF_FONT_SIZE,
          color: Colors.black,
        ),
        textAlign: TextAlign.left,
      ),
    );
    return Row(children: children);
  }

  Widget _randomAndTotal() {
    final children = <Widget>[];
    children.add(
      Text(
        data.displayRandomNumber ?? '',
        maxLines: 1,
        softWrap: false,
        textAlign: TextAlign.left,
        style: const TextStyle(
          fontSize: _DEF_FONT_SIZE,
          color: Colors.black,
        ),
      ),
    );
    children.add(
      Expanded(
        child: Text(
          data.displayTotalAmount ?? '',
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
          maxLines: 1,
          softWrap: false,
          textAlign: TextAlign.right,
        ),
      ),
    );
    return Row(children: children);
  }

  Widget _sellerAndBuyer() {
    final children = <Widget>[];
    children.add(
      Expanded(
        child: Text(
          data.displaySeller ?? '',
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
          textAlign: TextAlign.left,
          maxLines: 1,
          softWrap: false,
        ),
      ),
    );
    children.add(
      Expanded(
        child: Text(
          data.displayBuyer ?? '',
          style: const TextStyle(
            fontSize: _DEF_FONT_SIZE,
            color: Colors.black,
          ),
          textAlign: TextAlign.right,
          maxLines: 1,
          softWrap: false,
        ),
      ),
    );
    return Row(children: children);
  }

  Widget _barCode() {
    return SizedBox(
      width: double.infinity,
      height: 60.0,
      child: SvgPicture.string(
        _dm.toSvg(
          data.barcode ?? '',
          width: 368,
          height: 60.0,
          drawText: false,
          // fontFamily: 'Libre Barcode 39',
        ),
      ),
    );
  }

  Widget _doubleQrCode() {
    final children = <Widget>[];
    children.add(
      Expanded(
        child: SvgPicture.string(
          // leftQrSvgString,
          _qr.toSvg(
            data.leftQrString ?? '',
            height: 150.0,
            width: 150.0,
          ),
          height: 150.0,
        ),
      ),
    );
    children.add(SizedBox(width: 16.0));
    children.add(
      Expanded(
        child: SvgPicture.string(
          // rightQrSvgString,
          _qr.toSvg(
            data.rightQrString ?? '',
            height: 150.0,
            width: 150.0,
          ),
          height: 150.0,
        ),
      ),
    );
    return Row(children: children);
  }

  Widget _divider() {
    return SizedBox(height: 8.0);
  }
}
